package guru.springframework.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import guru.springframework.services.ProductService;

@Controller
public class IndexController {

	private ProductService productService;
	
	@Autowired
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/* The LoginController is redirecting to redirect:/index so the mapping of the getIndex method 
	 * was changed from @RequestMapping("/") to @RequestMapping({"/", "index"}) 
	 * 
	 */
	@RequestMapping({"/", "index"})
	public String getIndex(Model model) {
		model.addAttribute("products", productService.listProducts());
		return "index";
	}
	
	@RequestMapping("secured")
	public String secured() {
		return "secured";
	}
	
}
