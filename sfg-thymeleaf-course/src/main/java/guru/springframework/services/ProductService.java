package guru.springframework.services;

import java.util.List;

import guru.springframework.domain.Product;


public interface ProductService {

	Product getProduct(Integer id);
	
	List<Product> listProducts();
	
}
